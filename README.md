# Lost in Translation
An online translation app for translating written language into American sign language. 

Heroku link to Translation app: https://sleepy-plains-02108.herokuapp.com/

## Description
This online sign language translator was built as a Single Page Application using the React framework. The application consists of three parts:
- The startup page where the user can login
- The translation page where the user can input words to translate to American sign language
- The profile page which shows a users last 10 translations

The application uses an API to store each user and translations the user has done.

## Installation
Write the following in the terminal to install node moduels:
```
npm install
```

## Run the application
Write the following in the terminal to run the app:
```
npm start
```

### Startup page
On the startup page the user is able to enter their name and login. The username will be saved to the API. When the user clicks login they are redirected to the translation page. If a user is already logged in, they will automatically be redirected to the translation page. The logged in user will be stored in the browser session.

### Translation page
The translation page is only visible if a user is logged in. The user can write text in the input box and click "Translate" to view the translation. Images showing American sign language will be displayed. Special characters, numbers and spaces are ignored. The translations will be stored as text on the current user in the API. 

### Profile page
The user can view their profile page by clicking on "Profile" in the navigation bar on top of the screen. The profile page will display the user's last 10 translations in text form. The user can clear translations by clicking "Clear Translations". The translations will then be deleted from the API. The user can also go back to the translation page by clicking "Translate More".

### Logout button
The user can choose to logout at anytime by clicking the "Logout" button in the navigation bar on top of the screen. The session will be deleted and the user will be redirected to the startup screen. 

### API
A JSON server was used to store the data. Link:https://noroff-assignment-api-emoye.herokuapp.com/translations
There are three HTTP request actions:
- getUser (GET) which logs the user in
- register (POST) which registers a new user in the database if the user does not already exsist
- updateTranslations (PATCH) which updates the translations stores on the currently logged in user