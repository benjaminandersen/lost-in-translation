import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import LoginView from "./Components/Login/LoginView";
import TranslateView  from "./Components/Translate/TranslateView";
import ProfileView from "./Components/Profile/ProfileView";
import Navbar from "./Components/Navbar/Navbar";

function App() {
  return (
    <BrowserRouter>
      <Navbar/>
        <div className="App">
            <Switch>
              <Route path="/" exact component={LoginView} />
              <Route path="/translate" exact component={TranslateView} />
              <Route path="/profile" exact component={ProfileView} />
            </Switch>
        </div>
    </BrowserRouter>
  );
}

export default App;