import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { loginAttemptAction } from "../../store/actions/loginActions";
import AppContainer from "../../hoc/AppContainer";
import styles from "./Login.module.css";

const LoginView = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const currentSession = localStorage.getItem("rslt-ss");
  const { username } = useSelector((state) => state.sessionReducer);

  const { loginError, loginAttempting } = useSelector(
    (state) => state.loginReducer
  );

  const [credentials, setCredentials] = useState({
    username: "",
  });

  /**
   *
   * @param {*} event event is input from inputfield onchange
   */
  const onInputChange = (event) => {
    setCredentials({
      ...credentials,
      [event.target.id]: event.target.value,
    });
  };

  /**
   * useeffect for redirecting to translate view if user has session
   */
  useEffect(() => {
    if (currentSession !== null || username !== "") {
      history.push("/translate");
    }
  }, [currentSession, username]);

  /**
   *
   * @param {*} event event is input onclick that dispatches login action
   */
  const onFormSubmit = (event) => {
    event.preventDefault();
    dispatch(loginAttemptAction(credentials));
  };

  return (
    <AppContainer>
      <form className={styles.LoginPage} onSubmit={onFormSubmit}>
        <div className={styles.LoginContainer}>
          <h1 className={styles.LoginTitle}>
            Welcome to the Sign Language Translator!
          </h1>
          <div className="mt-3 mb-3">
            <label className={styles.Label}>Login here</label>
            <div className="col-5">
              <input
                id="username"
                type="text"
                placeholder="Enter username here"
                className="form-control"
                onChange={onInputChange}
              ></input>
            </div>
          </div>
          <button type="submit" className={styles.LoginButton}>
            Login
          </button>
        </div>
      </form>

      {loginAttempting && <p>Trying to login..</p>}

      {loginError && (
        <div className="alert alert-danger">
          <h4>Unsuccessful</h4>
          <p className="mb-0">{loginError}</p>
        </div>
      )}
    </AppContainer>
  );
};

export default LoginView;
