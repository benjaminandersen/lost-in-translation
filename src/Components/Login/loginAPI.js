const API_KEY =
  "9gBLjmloJSGc4aXYAgCecUmt4RZL1GOGy40wEQwCtQZzMXlgk8ZIWnaBNrOSvwCn";
const API_URL =
  "https://noroff-assignment-api-emoye.herokuapp.com/translations";

export const LoginAPI = {
  /**
   * register method to register new user to the API
   */
  async register(credentials) {
    return fetch(API_URL, {
      method: "POST",
      headers: {
        "X-API-Key": API_KEY,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(credentials),
    })
      .then(async (response) => {
        if (!response.ok) {
          throw new Error("Could not register user");
        }
        return await response.json();
      })
      .then((newUser) => {
        return newUser;
      })
      .catch((error) => {
        console.log(error);
      });
  },

  /**
   *
   * @param {*} credentials credetials is the users username from the login inputfiled
   * @returns returns a user array
   */
  async getUser(credentials) {
    return fetch(`${API_URL}?username=${credentials.username}`)
      .then(async (response) => {
        if (!response.ok) {
          throw new Error("Coud not get user");
        }
        return await response.json();
      })
      .then((results) => {
        return results;
      })
      .catch((error) => {
        console.log(error);
      });
  },

  /**
   *
   * @param {*} userId uses userId to update users transelations with patch
   * @param {*} translation new transelation array for the user with old + new transelation
   * @returns 
   */
  async updateTranslations(userId, translation) {
    return fetch(`${API_URL}/${userId}`, {
      method: "PATCH",
      headers: {
        "X-API-Key": API_KEY,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        translations: translation,
      }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Could not update translations history");
        }
        return response.json();
      })
      .then((updatedUser) => {
        return updatedUser;
      })
      .catch((error) => {
        console.log(error);
      });
  },
};
