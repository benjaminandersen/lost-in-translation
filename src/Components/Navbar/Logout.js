import { useDispatch } from "react-redux";
import { loginDeleteAction } from "../../store/actions/loginActions";
import { sessionDeleteAction } from "../../store/actions/sessionAction";
import { translateClearAction } from "../../store/actions/translateActions";
import styles from "./Navbar.module.css";

const Logout = () => {
  const dispatch = useDispatch();

  /**
   * clear session and sets initial state
   */
  const handleLogoutClick = () => {
    dispatch(translateClearAction());
    dispatch(sessionDeleteAction());
    dispatch(loginDeleteAction());
  };

  return (
    <div className={styles.Logout} onClick={handleLogoutClick}>
      Logout
    </div>
  );
};

export default Logout;
