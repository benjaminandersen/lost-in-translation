import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { useEffect } from "react";

import Logout from "./Logout";
import styles from "./Navbar.module.css";
import { translateClearAction } from "../../store/actions/translateActions";

const Navbar = () => {
  const { id, username } = useSelector((state) => state.sessionReducer);
  const history = useHistory();
  const dispatch = useDispatch();

  /**
   * clear transelate display when profile is clicked
   * redirect to profile when profile is clicked
   */
  const handleProfileClick = () => {
    dispatch(translateClearAction());
    history.push("/profile");
  };

  /**
   * redirects to login if user isnt logged in
   * checks on username from session
   */
  useEffect(() => {
    if (username === "") {
      history.push("/");
    }
  }, [username]);

  /**
   * shows username if user is logged in
   * checks on user id from session
   * @returns
   */
  const showUsername = () => {
    if (id) {
      return <div className={styles.Username}> {username} </div>;
    } else {
      return;
    }
  };
  /**
   * method to show logout button only if user is logged in
   * checks on user id from session
   * @returns
   */
  const showLogout = () => {
    if (id) {
      return <Logout />;
    } else {
      return;
    }
  };

  /**
   * method to show profile icon only if user is logged in
   * checks on user id from session
   * @returns
   */
  const showProfileIcon = () => {
    if (id) {
      return (
        <img
          className={styles.ProfileIcon}
          src="https://img.icons8.com/small/32/FFFFFF/user-male-circle.png"
          alt="profile-icon"
        />
      );
    } else {
      return;
    }
  };

  /**
   * method to show Profile link only if user is logged in
   * checks on user id from session
   * @returns
   */
  const showProfile = () => {
    if (id) {
      return (
        <div
          className={`${styles.NavbarMenuItem} ${styles.NavbarProfile}`}
          onClick={handleProfileClick}
        >
          Profile
        </div>
      );
    } else {
      return;
    }
  };

  return (
    <nav className={styles.Navbar}>
      <h1 className={styles.NavbarTitle}>Sign Language Translator</h1>
      {showUsername()}
      {showLogout()}
      {showProfileIcon()}
      {showProfile()}
    </nav>
  );
};

export default Navbar;
