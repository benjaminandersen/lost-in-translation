import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import styles from "./Profile.module.css";
import { useDispatch } from "react-redux";
import { translateDeleteAction } from "../../store/actions/translateActions";

const ProfileDisplay = () => {
  const { translations } = useSelector((state) => state.sessionReducer);
  const history = useHistory();
  const dispatch = useDispatch();
  /**
   * cleares users transelation list onclick
   */
  const handleClearClick = () => {
    dispatch(translateDeleteAction([]));
  };
  /**
   * redirects to translate view
   */
  const handleBackClick = () => {
    history.push("/translate");
  };
  return (
    <div className={styles.ProfilePage}>
      <div className={styles.TranslationContainer}>
        <h1 className={styles.TranslationTitle}> Translations </h1>
        <ul>
          {translations
            .map((translateItem, index) => {
              if (translateItem !== null) {
                const key = translateItem + index;
                return (
                  <li key={key} className={styles.TranslationList}>
                    {translateItem}
                  </li>
                );
              }
            })
            .slice(translations.length - 10, translations.length)}
        </ul>

        <button
          type="submit"
          className={styles.ProfileButton}
          onClick={handleClearClick}
        >
          Clear Translations
        </button>
        <button
          type="submit"
          className={styles.ProfileButton}
          onClick={handleBackClick}
        >
          Translate More
        </button>
      </div>
    </div>
  );
};

export default ProfileDisplay;
