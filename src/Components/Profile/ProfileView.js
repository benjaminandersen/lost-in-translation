import ProfileDisplay from "./ProfileDisplay";
import styles from "./Profile.module.css";

const ProfileView = () => {
  return (
    <div>
      <h1 className={styles.ProfileTitle}> Profile Page </h1>
      <ProfileDisplay />
    </div>
  );
};

export default ProfileView;
