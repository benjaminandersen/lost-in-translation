import { useSelector } from "react-redux";

/**
 *
 * maps word form translateReducer to images, then displays them in a list
 * @returns returns list
 */
const SignLanguage = () => {
  const { translation } = useSelector((state) => state.translateReducer);

  const translator = () => {
    if (translation != null) {
      return [...translation].map((char, index) => {
        if (char === " ") {
          return <span></span>;
        }
        return (
          <img key={index} src={`/individual_signs/${char}.png`} alt={char} />
        );
      });
    }
  };

  return (
    <div>
      <ul>{translator()}</ul>
    </div>
  );
};

export default SignLanguage;
