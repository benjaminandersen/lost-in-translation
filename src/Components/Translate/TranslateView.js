import AppContainer from "../../hoc/AppContainer"
import Translation from "./Translation"

const TranslateView = () => {
  return (
    <AppContainer>
      <Translation/>
    </AppContainer>
  )
}

export default TranslateView;