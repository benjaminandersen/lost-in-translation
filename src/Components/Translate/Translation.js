import { useState } from "react";
import { useDispatch } from "react-redux";
import { translateSetAction } from "../../store/actions/translateActions";
import AppContainer from "../../hoc/AppContainer";
import SignLanguage from "./SignLanguage";
import styles from "./Translate.module.css";

const Translation = () => {
  const [translation, setTranslations] = useState("");
  const dispatch = useDispatch();
  const onInputChange = (event) => {
    setTranslations(event.target.value);
  };
  /**
   *
   * @param {*} event is word to transelate
   * then sets it on state
   */
  const onFormSubmit = (event) => {
    event.preventDefault();
    setTranslations(event.target.value);
    dispatch(translateSetAction(translation.toLowerCase()));
  };

  return (
    <AppContainer>
      <form className={styles.TranslatePage} onSubmit={onFormSubmit}>
        <div>
          <h1 className={styles.TranslateTitle}>
            What do you want to translate?
          </h1>
          <div className="mt-3 mb-3">
            <label htmlFor="" className={styles.Label}>
              Translate below
            </label>
            <div className="col-12">
              <input
                id="translation"
                type="text"
                placeholder="Translate to sign language"
                className="form-control"
                onChange={onInputChange}
              ></input>
            </div>
            <button type="submit" className={styles.TranslateButton}>
              Translate
            </button>
          </div>
          <SignLanguage />
        </div>
      </form>
    </AppContainer>
  );
};

export default Translation;
