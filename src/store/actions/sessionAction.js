export const ACTION_SESSION_SET = "[session] SET";
export const ACTION_SESSION_DELETE = "[session] DELETE";

export const sessionSetAction = profile => ({
    type: ACTION_SESSION_SET,
    payload: profile
});

export const sessionDeleteAction = () => ({
    type: ACTION_SESSION_DELETE
});