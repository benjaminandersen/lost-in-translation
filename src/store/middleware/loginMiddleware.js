import { LoginAPI } from "../../Components/Login/loginAPI";
import {
  ACTION_LOGIN_ATTEMPTING,
  ACTION_LOGIN_SUCCESS,
  loginErrorAction,
  loginSuccessAction,
} from "../actions/loginActions";
import { sessionSetAction } from "../actions/sessionAction";

export const loginMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);
    /**
     * logs in if username exists, else
     * creates new user
     */
    if (action.type === ACTION_LOGIN_ATTEMPTING) {
      LoginAPI.getUser(action.payload)
        .then((profile) => {
          if (profile.length === 0) {
            LoginAPI.register(action.payload)
              .then((prof) => {
                dispatch(loginSuccessAction(prof));
              })
              .catch((error) => {
                dispatch(loginErrorAction(error.message));
              });
          }

          dispatch(loginSuccessAction(profile));
        })
        .catch((error) => {
          dispatch(loginErrorAction(error.message));
        });
    }
    /**
     * sest session if login success
     * creates array for session object
     */
    if (action.type === ACTION_LOGIN_SUCCESS) {
      if (Array.isArray(action.payload)) {
        dispatch(sessionSetAction(action.payload));
      } else {
        dispatch(sessionSetAction([action.payload]));
      }
    }
  };
