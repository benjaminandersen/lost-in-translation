import { ACTION_SESSION_SET } from "../actions/sessionAction"
import { ACTION_SESSION_DELETE } from "../actions/sessionAction"
import { translateSetAction } from "../actions/translateActions"

export const sessionMiddleware = ({ dispatch }) => next => action => {
    next(action)
    /**
     * creates localstoreg item
     */
    if (action.type === ACTION_SESSION_SET) {
        localStorage.setItem("rslt-ss", JSON.stringify(action.payload))
    }
    /**
     * clears localstorage
     */
    if(action.type === ACTION_SESSION_DELETE) {
        localStorage.clear();
    }
}