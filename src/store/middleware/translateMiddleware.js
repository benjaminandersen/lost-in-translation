import { LoginAPI } from "../../Components/Login/loginAPI";
import {
  ACTION_TRANSLATE_DELETE,
  ACTION_TRANSLATE_SET,
  ACTION_TRANSLATE_SUCCESS,
  translateErrorAction,
} from "../actions/translateActions";
import { sessionSetAction } from "../actions/sessionAction";

export const translateMiddleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action);

    /**
     * gets users translation from session, if user has translations, push new translation else
     * create new array with new transelation
     * then gets the user again and updates session with updated user
     */
    if (action.type === ACTION_TRANSLATE_SET) {
        const currentSession = localStorage.getItem("rslt-ss");
        const session = JSON.parse(currentSession);
        let temp = session[0].translations;

        if (temp === undefined) {
          temp = [action.payload];
        } else {
          session[0].translations.push(action.payload);
        }

        await LoginAPI.updateTranslations(session[0].id, temp);
        await LoginAPI.getUser({ username: session[0].username })
          .then((profile) => {
            dispatch(sessionSetAction(profile));
          })
          .catch((error) => {
            dispatch(translateErrorAction(error.message));
          });
    }

    if (action.type === ACTION_TRANSLATE_SUCCESS) {
      dispatch(sessionSetAction(action.payload));
    }
    /**
     * deletes users transelations and gets updated user.
     */
    if (action.type === ACTION_TRANSLATE_DELETE) {
      const currentSession = localStorage.getItem("rslt-ss");
      const session = JSON.parse(currentSession);
      await LoginAPI.updateTranslations(session[0].id, []);
      await LoginAPI.getUser({ username: session[0].username })
        .then((user) => {
          dispatch(sessionSetAction(user));
        })
        .catch((error) => {
          dispatch(translateErrorAction(error.message));
        });
    }
  };
