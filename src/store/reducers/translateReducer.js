import { ACTION_TRANSLATE_DELETE, ACTION_TRANSLATE_SET, ACTION_TRANSLATE_CLEAR } from "../actions/translateActions";

const initialState = {
    translation: []
}

export const translateReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TRANSLATE_SET:
            return {
                ...state,
                translation: action.payload
            }
        case ACTION_TRANSLATE_DELETE:
            return {
                ...state,
                translation: action.payload
            }
        case ACTION_TRANSLATE_CLEAR:
                return {
                    ...initialState,
                    translation: action.payload
                }
        default:
            return state;
    }
}